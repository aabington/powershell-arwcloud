## 0_Server-Build.ps1  PowerShell script to build and configure server
## Created by Art Abington 12/28/2015
## The installation files for anti-virus, etc. are stored on \\inordrsw000\c$\Installs
## This script is for the dev SiteCore server scordrsw001
## This script should be placed in a C:\Installs folder on the target server.
## -----start-----  After features were tested and installed, I have commented the lines out
##                    with a single # to develop and test the next step.
##                  Uncomment lines with a single # to install features on a new server. 
##                  Leave ## lines in place.
##---Check connection to software repository
if((Test-Path "\\inordrsw001\c$\Installs\dont-delete.txt") -eq $false) 
{
  write-host "A connection to the file repository on server INORDRSW001 is not available." -ForegroundColor "yellow"
  write-host "  1. Run this script with an account that has Local Administrator and "  -ForegroundColor "yellow"
  write-host "     read rights to \\INORDRSW001\c$\Installs\.  Domain Admin rights suggested."  -ForegroundColor "yellow"
  write-host "  2. Join this server to the domain ARWCLOUD.NET if you have not yet done so." -ForegroundColor "yellow"
  write-host "  3. Move this computer object to the correct OU to apply the proper Group Policies."-ForegroundColor "yellow"
  write-host "  4. You may need to open network ports to connect to the software repository." -ForegroundColor "yellow"
  write-host "Aborting install." -ForegroundColor "yellow"
}
else 
{
##
##---Start Installation
write-host "Software repository available. Proceeding with install."  -ForegroundColor "yellow"
#Update-Help
#Set-ExecutionPolicy -Scope LocalMachine -ExecutionPolicy RemoteSigned -Force
Import-Module ServerManager
##
#Add-WindowsFeature RSAT-AD-PowerShell
#Import-Module ActiveDirectory
##
##---Pull Down Group Policy and Register with Windows Update Server
$program = "c:\windows\system32\gpupdate.exe"
$arguments = "/force"
Start-Process -FilePath "$program" -ArgumentList "$arguments" -Wait -PassThru
write-host "Waiting 5 seconds."
sleep 5
$program = "C:\windows\system32\wuauclt.exe"
$arguments = "/ResetAuthorization /DetectNow"
Start-Process -FilePath "$program" -ArgumentList "$arguments" -Wait -PassThru
write-host "Waiting 15 seconds."
sleep 15
##
##---Install BGInfo - Back Ground Information for screen
#md "c:\Program Files (x86)\BGInfo"
#copy \\inordrsw001\c$\installs\BGInfo\*.* "c:\Program Files (x86)\BgInfo\"
copy "C:\Program Files (x86)\BGInfo\BGI-Start.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\"
##
##---Install Sophos AV
#md "c:\Installs\Sophos-AV"
#copy \\inordrsw001\c$\installs\Sophos-AV\*.* "c:\installs\Sophos-AV\"
#$program = "c:\windows\system32\gpupate.exe"
#$arguments = "-force"
#Start-Process -FilePath "$program" -ArgumentList "$arguments" -Wait -PassThru
##
##---Install New Relic Server Agent
#md "c:\Installs\NewRelicAgent"
#copy \\inordrsw001\c$\installs\NewRelicAgent\*.* "c:\installs\NewRelicAgent\"
#$program = "C:\Installs\NewRelicAgent\NewRelicServerMonitor_x64_3.3.3.0.msi"
#$arguments = '/L*v "C:\Installs\NewRelicAgent\nrsa-install.log" /qn NR_LICENSE_KEY="c97d9d904b263451482e33df88fa6882448ff371"'
#Start-Process -FilePath "$program" -ArgumentList "$arguments" -Wait -PassThru
##
##---Install IIS and All Features with Managment Tools
#add-windowsfeature web-server -IncludeAllSubFeature -IncludeManagementTools
##---Remove FTP services If Not Needed
#uninstall-windowsfeature Web-Ftp-Server
##
##---Install New Relic .NET agent
#Stop-Service W3SVC
#Get-Service W3SVC
#$program = "C:\Installs\NewRelicAgent\NewRelicAgent_x64_5.4.16.0.msi"
#$arguments = "/qb NR_LICENSE_KEY=<c97d9d904b263451482e33df88fa6882448ff371> INSTALLLEVEL=1"
#Start-Process -FilePath "$program" -ArgumentList "$arguments" -Wait -PassThru
#Start-Service W3SVC
#Get-Service W3SVC
##
##---Install Windows Update Module for PowerShell
#md "c:\installs\PSWindowsUpdate"
#copy \\inordrsw001\c$\installs\PSWinUpdate\*.* "c:\installs\PSWindowsUpdate\"
#import-module "c:\installs\PSWindowsUpdate"
#get-WUInstall -AcceptAll -AutoReboot
}
