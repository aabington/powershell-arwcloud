## trim-logs.ps1 by Art Abington  12-29-2015
## For Windows IIS web servers
## Allows trimming log files from directories to keep only a set number of days of logs from up to 4 directories
## Script is launched with a Windows CMD file. Run this CMD file as a scheduled task to run every day
## Script must reside in C:\Scripts\Trim-Logs\
## The script also keeps a log of the times it ran in CL\Scripts\Trim-Logs\logs and keeps those trimmed also
## The log file contains the free space on both drives C: and D: for a historical record

# Define parameters: number of days of logs to retain, log folder names, and whether to include sub-folders
$Days = "7"
$TF = "4"   # Number of target folders to process
$TargetFolder1 = "D:\Octopus\Applications\Prod-CD-LON\Arrow.Site.Release\Data\logs\"
$TargetFolder2 = "D:\Octopus\Applications\Prod-CD-LON\Arrow.Site.Release\logs\"
$TargetFolder3 = "D:\Octopus\Applications\LON Production Sitecore Delivery Cluster\Arrow.Site\Data\logs\"
$TargetFolder4 = "C:\inetpub\logs\LogFiles\W3SVC2\"

$RecurseFolders = "N"  # Replace n with capital Y if you want to include subfolder recusion

# Generate name for output file
$LogTime = Get-Date -Format "yyyy-MM-dd_hh.mm.ss"
$LogFile = 'C:\Scripts\Trim-Logs\Logs\'+"Trim-Logs_"+$LogTime+".log"

# Get current date and define LastWriteTime parameter based on $Days
$Now = Get-Date
$LastWrite = $Now.AddDays(-$Days)

$TimeStamp = Get-Date
#Write timestamp to log file.  ARWCLOUD.NET servers are set to UTC time so that is why the reference below
write "- - - - - Trim-Logs.ps1 ran at UTC time: $TimeStamp - - - - - " >> $LogFile

# Get free space on drives
$FreeC = (Get-WmiObject -class win32_LogicalDisk -filter "DeviceID = 'C:'").FreeSpace
$FreeC = $FreeC/1GB
$FreeC = "{0:N2}" -f $FreeC
$FreeD = (Get-WmiObject -class win32_LogicalDisk -filter "DeviceID = 'D:'").FreeSpace
$FreeD = $FreeD/1GB
$FreeD = "{0:N2}" -f $FreeD

# Write free space to log file before trimming logs
write "  Free space before log trim C: $FreeC D: $FreeD" >> $LogFile

#process TargetFolder1
#get files to remove based on lastwrite filter and specified folder
if ($RecurseFolders = "Y")
  { 
        $Files = Get-Childitem $TargetFolder1 -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
  } else
  {
        $Files = Get-Childitem $TargetFolder1 | Where {$_.LastWriteTime -le "$LastWrite"}
  }

#write-host $files

foreach ($File in $Files)
  {
#following line is for troubleshooting - comment out for production
#        write-host "Deleting File $File" -ForegroundColor "Red"
        Remove-Item $File.FullName | out-null
  }

If ($TF -gt 1) {
#process TargetFolder2
#get files to remove based on lastwrite filter and specified folder
if ($RecurseFolders = "Y")
  { 
        $Files = Get-Childitem $TargetFolder2 -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
  } else
  {
        $Files = Get-Childitem $TargetFolder2 | Where {$_.LastWriteTime -le "$LastWrite"}
  }

#write-host $files

foreach ($File in $Files)
  {
#following line is for troubleshooting - comment out for production
#        write-host "Deleting File $File" -ForegroundColor "Red"
        Remove-Item $File.FullName | out-null
  }
}

If ($TF -gt 2) {
#process TargetFolder3
#get files to remove based on lastwrite filter and specified folder
if ($RecurseFolders = "Y")
  { 
        $Files = Get-Childitem $TargetFolder3 -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
  } else
  {
        $Files = Get-Childitem $TargetFolder3 | Where {$_.LastWriteTime -le "$LastWrite"}
  }

#write-host $files

foreach ($File in $Files)
  {
#following line is for troubleshooting - comment out for production
#        write-host "Deleting File $File" -ForegroundColor "Red"
        Remove-Item $File.FullName | out-null
  }
}

If ($TF -gt 3) {
#process TargetFolder4
#get files to remove based on lastwrite filter and specified folder
if ($RecurseFolders = "Y")
  { 
        $Files = Get-Childitem $TargetFolder4 -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
  } else
  {
        $Files = Get-Childitem $TargetFolder4 | Where {$_.LastWriteTime -le "$LastWrite"}
  }

#write-host $files

foreach ($File in $Files)
  {
#following line is for troubleshooting - comment out for production
#        write-host "Deleting File $File" -ForegroundColor "Red"
        Remove-Item $File.FullName | out-null
  }
}

# Get free space on drives after trimming logs
$FreeC = (Get-WmiObject -class win32_LogicalDisk -filter "DeviceID = 'C:'").FreeSpace
$FreeC = $FreeC/1GB
$FreeC = "{0:N2}" -f $FreeC
$FreeD = (Get-WmiObject -class win32_LogicalDisk -filter "DeviceID = 'D:'").FreeSpace
$FreeD = $FreeD/1GB
$FreeD = "{0:N2}" -f $FreeD

# Write free space to log file
write "  Free space after log trim  C: $FreeC D: $FreeD" >> $LogFile

# Clean up trim-logs.log files older than 30 days
$days = "30"
$LastWrite = $Now.AddDays(-$Days)
#get files to remove based on lastwrite filter and specified folder
$Files = Get-Childitem "C:\Scripts\trim-logs\logs\" | Where {$_.LastWriteTime -le "$LastWrite"}
foreach ($File in $Files)
  {
        Remove-Item $File.FullName | out-null
  }
 