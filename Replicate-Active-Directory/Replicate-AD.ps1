## Replicate-AD.ps1 PowerShell script to replicate Active Directory database between domain controllers
## By Art Abington 11/03/2015
## This script uses Dell ActiveRoles snap-in for Powershell found here: \\inordrsw000\c$\Installs\Dell-ActiveRoles
## The script will find all existing domain controllers and attempt replication

# Begin by importing the ActiveDirectory module and adding the snap-in
import-module activedirectory
Add-PSSnapin Quest.ActiveRoles.ADManagement

# Transcribe output to log
$null = Start-Transcript "$pwd\$([System.IO.Path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Definition)).log"

# Check the QAD snapins are installed
if ( (Get-PSSnapin -Name Quest.ActiveRoles.ADManagement -ErrorAction silentlycontinue) -eq $null ) {

# Check to see if the QAD snapin is installed.  Give error message if not.  If installed add the snap-in
  if ( (Get-PSSnapin -Name Quest.ActiveRoles.ADManagement -Registered -ErrorAction SilentlyContinue) -eq $null) {
    Write-Error "You must install Quest ActiveRoles AD Tools to use this script!"
  } else {
    Write-Host "Importing QAD Tools"
    Add-PSSnapin -Name Quest.ActiveRoles.ADManagement -ErrorAction Stop
  }
}

Write-Host " "
Write-Host " "
Write-Host "Beginning AD Domain Services Replication"
Write-Host "========================================"

# Find each domain controller, then do a foreach-object
Get-QADComputer -ComputerRole 'DomainController' | % {
  Write-Host "Replicating $($_.Name)"

# Recalculate topology for this server
  $null = repadmin /kcc $_.Name

# Replicate it
  $null = repadmin /syncall /A /e $_.Name
}

Write-Host "========================================"
Write-Host "Completed AD Domain Services Replication"
Write-Host " "
write-Host "This script will exit in 10 seconds."

Stop-Transcript > $NULL

Start-Sleep -s 10

